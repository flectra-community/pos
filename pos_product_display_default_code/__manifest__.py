{
    "name": "PoS Product Display Default Code",
    "summary": "pos: display product default code before product name",
    "version": "2.0.1.0.1",
    "category": "Point of Sale",
    "website": "https://gitlab.com/flectra-community/pos",
    "author": "Odoo Community Association (OCA), Akretion",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "point_of_sale",
    ],
    "data": ["templates/assets.xml"],
    "demo": [],
    "qweb": [],
}

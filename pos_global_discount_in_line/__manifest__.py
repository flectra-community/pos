{
    "name": "Point of Sale - Global Discount in Line",
    "summary": "Order discount in line instead of discount product",
    "version": "2.0.1.0.0",
    "category": "Point Of Sale",
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/pos",
    "license": "AGPL-3",
    "depends": [
        "pos_discount",
    ],
    "data": ["views/pos_templates.xml", "views/pos_discount_views.xml"],
}

# Flectra Community / pos

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[pos_customer_required](pos_customer_required/) | 2.0.1.0.0| Point of Sale Require Customer
[pos_payment_change](pos_payment_change/) | 2.0.1.0.2| Allow cashier to change order payments, as long as the session is not closed.
[pos_default_partner](pos_default_partner/) | 2.0.1.0.0| Add a default customer in pos order
[pos_empty_home](pos_empty_home/) | 2.0.1.0.0| Point of Sale - Hide products if no category is selected
[pos_receipt_hide_price](pos_receipt_hide_price/) | 2.0.1.0.1| Add button to remove price from receipt.
[pos_supplierinfo_barcode](pos_supplierinfo_barcode/) | 2.0.1.0.0| Search products by supplier barcode
[pos_session_pay_invoice](pos_session_pay_invoice/) | 2.0.1.1.0| Pay and receive invoices from PoS Session
[pos_report_discount](pos_report_discount/) | 2.0.1.0.0| POS Report Discount
[pos_fixed_discount](pos_fixed_discount/) | 2.0.1.0.2| Allow to apply discounts with fixed amount
[pos_customer_display](pos_customer_display/) | 2.0.1.0.1| Manage LED Customer Display device from POS front end
[pos_product_multi_barcode](pos_product_multi_barcode/) | 2.0.1.0.2| Make product multi barcodes usable in the point of sale
[pos_reset_search](pos_reset_search/) | 2.0.1.0.1| Point of Sale - Clear product search when user clicks on a product.
[pos_edit_order_line](pos_edit_order_line/) | 2.0.1.0.2| POS Edit Order Line
[pos_reuse_pricelist](pos_reuse_pricelist/) | 2.0.1.0.1| PoS reuse pricelist
[pos_order_to_sale_order](pos_order_to_sale_order/) | 2.0.1.0.0| PoS Order To Sale Order
[pos_require_product_quantity](pos_require_product_quantity/) | 2.0.1.0.0|         A popup is shown if product quantity is set to 0 for one or more order        lines when clicking on "Payment" button.    
[pos_supplierinfo_search](pos_supplierinfo_search/) | 2.0.1.0.0| Search products by supplier data
[pos_no_cash_bank_statement](pos_no_cash_bank_statement/) | 2.0.1.0.3| Generate bank statements for all payment methods, not only cash
[pos_warning_exiting](pos_warning_exiting/) | 2.0.1.0.0| Add warning at exiting the PoS front office UI if there are pending draft orders
[pos_order_mgmt](pos_order_mgmt/) | 2.0.1.0.1| Manage old POS Orders from the frontend
[pos_product_sort](pos_product_sort/) | 2.0.1.0.0|         sort the products by name in the point of sale        instead of sorting them by the sequence field.        
[pos_product_template_configurator](pos_product_template_configurator/) | 2.0.1.0.0| Manage Product Template in Front End Point Of Sale via Configurator
[pos_order_remove_line](pos_order_remove_line/) | 2.0.1.0.0| Add button to remove POS order line.
[pos_show_config_name](pos_show_config_name/) | 2.0.1.0.0| Point of sale: show pos config name
[pos_order_product_search](pos_order_product_search/) | 2.0.1.0.0| Search for orders by product fields
[pos_order_return](pos_order_return/) | 2.0.1.0.3| Point of Sale Order Return
[pos_product_template](pos_product_template/) | 2.0.1.0.2| Manage Product Template in Front End Point Of Sale
[pos_payment_terminal](pos_payment_terminal/) | 2.0.2.1.0| Point of sale: support generic payment terminal
[pos_margin](pos_margin/) | 2.0.1.0.3| Margin on PoS Order
[pos_partner_birthdate](pos_partner_birthdate/) | 2.0.1.0.2| Adds the birthdate in the customer screen of POS
[pos_show_clock](pos_show_clock/) | 2.0.1.0.0| Point of Sale: Display Current Date and Time on POS sreen
[pos_report_session_summary](pos_report_session_summary/) | 2.0.1.0.2| Adds a Session Summary PDF report on the POS session
[pos_payment_method_cashdro](pos_payment_method_cashdro/) | 2.0.1.0.1| Allows to pay with CashDro Terminals on the Point of Sale
[pos_ticket_without_price](pos_ticket_without_price/) | 2.0.1.0.0| Adds receipt ticket without price or taxes
[pos_partner_firstname](pos_partner_firstname/) | 2.0.1.0.1| POS Support of partner firstname
[pos_backend_communication](pos_backend_communication/) | 2.0.1.0.1| Communicate with flectra's backend from POS.
[pos_product_display_default_code](pos_product_display_default_code/) | 2.0.1.0.1| pos: display product default code before product name
[pos_user_restriction](pos_user_restriction/) | 2.0.1.0.0| Restrict some users to see and use only certain points of sale
[pos_cash_move_reason](pos_cash_move_reason/) | 2.0.1.1.1| POS cash in-out reason
[pos_escpos_status](pos_escpos_status/) | 2.0.1.0.0| Point of sale: fetch status for 'escpos' driver
[pos_hide_banknote_button](pos_hide_banknote_button/) | 2.0.1.0.1| Hide useless Banknote buttons in the PoS (+10, +20, +50)
[pos_timeout](pos_timeout/) | 2.0.1.0.0| Set the timeout of the point of sale
[pos_customer_tree_view_vat](pos_customer_tree_view_vat/) | 2.0.1.0.1| Point of Sale: Show VAT number at Customer Tree View
[pos_global_discount_in_line](pos_global_discount_in_line/) | 2.0.1.0.0| Order discount in line instead of discount product
[pos_access_right](pos_access_right/) | 2.0.1.0.2| Point of Sale - Extra Access Right for certain actions
[pos_disable_pricelist_selection](pos_disable_pricelist_selection/) | 2.0.1.0.1| Disable Pricelist selection button in POS



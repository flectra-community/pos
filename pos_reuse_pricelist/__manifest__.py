# Copyright 2021 Akretion (https://www.akretion.com).
# @author Pierrick Brun <pierrick.brun@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "PoS reuse pricelist",
    "version": "2.0.1.0.1",
    "category": "Point Of Sale",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/pos",
    "depends": [
        "point_of_sale",
    ],
    "data": [
        "views/pos_assets.xml",
    ],
    "demo": [],
    "installable": True,
}

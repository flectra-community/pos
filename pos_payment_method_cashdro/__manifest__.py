# Copyright 2021 Tecnativa - David Vidal
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "PoS Payment Method CashDro",
    "summary": "Allows to pay with CashDro Terminals on the Point of Sale",
    "version": "2.0.1.0.1",
    "category": "Point Of Sale",
    "website": "https://gitlab.com/flectra-community/pos",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": [
        "point_of_sale",
    ],
    "data": [
        "views/assets.xml",
        "views/pos_payment_method_views.xml",
    ],
    "installable": True,
}

{
    "name": "POS Report Discount",
    "summary": "POS Report Discount",
    "version": "2.0.1.0.0",
    "category": "Point Of Sale",
    "maintainers": ["dessanhemrayev", "CetmixGitDrone"],
    "website": "https://gitlab.com/flectra-community/pos",
    "author": "Cetmix, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "auto_install": False,
    "depends": [
        "point_of_sale",
    ],
    "data": [
        "views/report_pos_order.xml",
    ],
}
